<?php
class User
{
    private $id;
    private $password;
    private $email;

    public function __construct($id, $password, $email) {
        try {
            $this->setId($id);
            $this->setPassword($password);
            $this->email = $email;
        } catch (Exception $error) {
            echo $error->getMessage();
            echo $error->getFile();
            echo $error->getLine();
        }
    }

    private function setId($id) {
        if(is_int($id)) {                  //Проверяем, является ли значение integer
            return $this->id = $id;
        } else {
            throw new InvalidArgumentException('Integer needed!');
        }
    }

    private function setPassword($password) {
        if(strlen($password) > 8) {              //Условие, если количество символов превышает 8
            throw new LengthException('Password must have no more than 8 characters!');
        } else {
            return $this->password = $password;
        }
    }

    public function getUserData() {
        echo 'Id пользователя: ' . $this->id;
        echo'<br/>';
        echo 'Адрес электронной почты: ' . $this->email;
        echo'<br/>';
    }
}
?>